# Description #
Enable user to view existing and schedule new cron jobs
Cron job sends a specified message to selected Slack channel at specified time

Focus for this assignment is exclusively on functionality, clean and reusable code in React and Redux.
 
If you have your own boilerplate feel free to use it otherwise you can use create react app or any other boilerplate.

When submitting the assignment please provide us with information how much time you spent on it. If for some reason you are not able to resolve all tasks from the list submit the ones you did. 

If you have any questions feel free to contact us at **ljerinic@iolap.com**

# Deliverables #
### UI ###
*	Entire frontend must be made with React and Redux
*	Frontend should have:
	*	Table with a view of schedules cron jobs:
		*	Message
		*	Time
		*	Channel
		*	Status (optional)
	*	Input form
		*	Time picker
		*	Channel picker
		*	Message input
*	Cron job deletion option
### BACKEND ###
*	Uses Express framework
*	Serves logic (JS) and static data if it exists (HTML and CSS) to clients
*	Uses GraphQL API for serving data
*	Schedules asynchronous cron jobs with Agenda
	*	Input validation
*	Slack interaction
	*	Fetches available channels
	*	Sends messages to specified channel
*	Updates jobs status depending on success of Slack interaction

### REFERENCES ###
*	Starter kit available for use (bonus points if not used):
	*	https://github.com/facebook/create-react-app
*	Useful links:
	*	https://nodejs.org
	*	https://github.com/expressjs/express
	*	https://github.com/slackapi/node-slack-sdk
	*	https://github.com/facebook/react
	*	https://github.com/rschmukler/agenda
	*	https://www.mongodb.com/
	*	https://mlab.com/

